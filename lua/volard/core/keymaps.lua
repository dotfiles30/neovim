local utils = require('volard.core.utils')

local M = {}
-- TODO t fuck is it
-- { shortcut = '?', cmd = '?\\v', opts = no_remap_opt, modes = { 'n' }, description = 'Improve search' },
--     { shortcut = '/', cmd = '/\\v', opts = no_remap_opt, modes = { 'n' }, description = 'Improve search' },
--     { shortcut = '\\', cmd = '/@', opts = no_remap_opt, modes = { 'n' }, description = 'Improve search' },
--     { shortcut = '%s/', cmd = '%sm/', opts = no_remap_opt, modes = { 'c' }, description = 'Improve search' },





--[[
  [[ Base keymap for non plugin specific shortcuts
	[[   

	Template

	{
		[ desc = "Description of a shortcut", ]
		key  = "<leader>gg",
		action = function()
			return require('volard.core.utils').execute_with_terminal('lazygit', 'float')
		end,
		modes = { "n", "i" },
		opts = { silent = true },
		set_with_nvim_set_keymap = true,
	},

	By default in setup() function shortcuts are created using vim.keymap.set, but
	as I've discovered some actions works better (for me its legendary.find() for
	some reason idk) when they are set with vim.api.nvim_set_keymap(). For that reason
	there is a optional set_with_nvim_set_keymap boolean field

	To document default keymap for yourself u can just leave desc field alone,

	Action can be a lua function or lua string

]]

M.base_keymap = {
	{
		desc = "Start insert before current word",
		key = "<C-b>"
	},
	{
		desc = "Find cpp reference",
		key = "<leader>fcr",
		action = function()
			return utils.find_cpp_reference(vim.call('expand', '<cword>'))
		end,
		modes = { "n" }
	},
	{
		desc = "Open telescope file browser (Telescope Files)",
		key = "<leader>tf",
		action = "<cmd>Telescope file_browser<CR>",
		modes = { "n" }
	},
	{
		desc = "Close all windows except current (good to fix floatings)",
		key = "<C-w-o>"
	},
	{
		desc = "Open vifm in current directory",
		key = "<leader>v",
		action = function()
			return utils.exec_in_term('vifm .', 'float', true)
		end,
		modes = { "n" },
	},
	{
		desc = "Open new terminal in new tab",
		key = "<A-'",
		action = function()
			return utils.TERMTAB_OPEN()
		end,
		modes = { "n" },
	},
	{
		desc = "Jump to next error",
		key = "]e",
		action = function()
			return require("lspsaga.diagnostic").goto_next({ severity = vim.diagnostic.severity.ERROR })
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Jump to previous error",
		key = "[e",
		action = function()
			return require("lspsaga.diagnostic").goto_prev({ severity = vim.diagnostic.severity.ERROR })
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Jump to previous diagnostic",
		key = "[d",
		action = function()
			return require("lspsaga.diagnostic").goto_prev({ severity = vim.diagnostic.severity.DIAGNOSTICS })
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Jump to next diagnostic",
		key = "]d",
		action = function()
			return require("lspsaga.diagnostic").goto_next({ severity = vim.diagnostic.severity.DIAGNOSTICS })
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Scroll down hover doc in definition preview",
		key = "<C-f>",
		action = function()
			require('lspsaga.action').smart_scroll_with_saga(1)
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Scroll up hover doc in definition preview",
		key = "<C-b>",
		action = function()
			return require('lspsaga.action').smart_scroll_with_saga(-1)
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Delete word with Ctrl+Backspace",
		key = "<C-BS>",
		action = "<C-w>",
		modes = { "i" },
	},
	{
		desc = "Open new buffer",
		key = "<S-b>",
		action = "<cmd> enew <CR>",
		modes = { "n" },
	},
	{
		desc = "Copy all file",
		key = "<C-c>",
		action = "<cmd>%y+<CR>",
		modes = { "n" },
	},
	-- Some insert mode tricks
	{
		desc = "Go to beginning of current line",
		key = "<C-b>",
		action = "<esc>^i",
		modes = { "n" },
	},
	{
		desc = "Go to end of current line",
		key = "<C-e>",
		action = "<End>",
		modes = { "n" },
	},
	{
		desc = "Go to normal mode",
		key = "<A-;>",
		action = "<esc>",
		modes = { "i", "v" },
	},
	{
		desc = "Go to normal mode",
		key = "<C-;>",
		action = "<esc>",
		modes = { "i", "v" },
	},
	{
		desc = "Go to normal mode",
		key = "<C-l>",
		action = "<esc>",
		modes = { "i", "v" },
	},
	{
		desc = "Toggle Lsoutline menu",
		key = "<S-t>",
		action = "<cmd>LSoutlineToggle<CR>",
		modes = { "n" },
	},

	-- Jump by visual lines instead of logical lines
	{
		key = "j",
		action = "gj",
		modes = { "n" }
	},
	{
		key = "k",
		action = "gk",
		modes = { "n" }
	},
	{
		desc = "Close current buffer with saving it",
		key = "<S-w>",
		action = "<esc>:w<cr><esc>:bdelete %<cr>",
		modes = { "n" }
	},
	{
		desc = "Go to command mode",
		key = ";",
		action = ":",
		modes = { "n" },
	},
	{
		desc = "Open Netrw",
		key = "<leader>pv",
		action = ":Ex<cr>",
		modes = { "n" },
	},
	{
		desc = "Show hover documentation",
		key = "K",
		action = "<cmd>Lspsaga hover_doc<CR>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Open lsp finder",
		key = "gh",
		action = "<cmd>Lspsaga lsp_finder<CR>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Open code actions",
		key = "<leader>ca",
		action = "<cmd>Lspsaga code_action<CR>",
		modes = { "n" },
		opts = { silent = true },
		set_with_nvim_set_keymap = true,
	},
	-- close rename win use <C-c> in insert mode or `q` in normal mode or `:q`
	{
		desc = "Rename item with lsp",
		key = "gr",
		action = "<cmd>Lspsaga rename<CR>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Preview definition",
		key = "gd",
		action = "<cmd>Lspsaga preview_definition<CR>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Close current buffer withOUT saving it",
		key = "<A-S-w>",
		action = "<esc>:bdelete %<cr>",
		modes = { "n" }
	},
	{
		desc   = "Toggle NvimTree",
		key    = "<C-n>",
		action = ":NvimTreeToggle<CR>",
		modes  = { "n", "i" },
	},
	{
		desc   = "Run python",
		key    = "<leader>rp",
		action = function()
			local current_file = vim.api.nvim_buf_get_name(0)
			return utils.exec_in_term(
				'python ' .. current_file, 'float', false)
		end,
		modes  = { "n" },
	},
	{
		desc   = "Run cpp",
		key    = "<leader>rcp",
		action = function()
			local current_file = vim.api.nvim_buf_get_name(0)
			return utils.exec_in_term(
				'g++ ' .. current_file .. " -o out.exe && out.exe", 'float', false)
		end,
		modes  = { "n" },
	},
	{
		desc   = "Run C#",
		key    = "<leader>rc",
		action = function()
			return utils.exec_in_term(
				'dotnet run', 'float', false)
		end,
		modes  = { "n" },
	},
	{
		desc = "Next opened tab",
		key = "<A-S-L>",
		action = "<cmd>tabnext<CR>",
		modes = { "n" },
	},
	{
		desc = "Previous opened tab",
		key = "<A-S-H>",
		action = ":tabprevious<CR>",
		modes = { "n" },
	},
	{
		desc = "New tab",
		key = "<leader><S-t>",
		action = ":tabnew<CR>",
		modes = { "n" }
	},
	{
		desc = "Close tab",
		key = "<leader><S-w>",
		action = ":tabclose<CR>",
		modes = { "n" }
	},
	{
		desc = "Toggle colorsheme (dark <-> light)",
		key = "<leader>mm",
		action = function()
			if vim.g.material_style == 'deep ocean' then
				return require('material.functions').change_style('lighter')
			end
			return require('material.functions').change_style('deep ocean')
		end,
		modes = { "n" },
		opts = { silent = true },
	},
	-- Make navigation easier
	{
		key = "<",
		action = "<gv",
		modes = { "v" },
	},
	{
		key = ">",
		action = ">gv",
		modes = { "v" },
	},
	-- Splits navigation
	{
		key = "<C-h>",
		action = ":wincmd h<CR>",
		modes = { "n" },
	},
	{
		key = "<C-k>",
		action = ":wincmd k<CR>",
		modes = { "n" },
	},
	{
		key = "<C-j>",
		action = ":wincmd j<CR>",
		modes = { "n" },
	},
	{
		key = "<C-l>",
		action = ":wincmd l<CR>",
		modes = { "n" },
	},
	-- Splits moving
	{
		key = "<leader><C-l>",
		action = ":wincmd <S-l><CR>",
		modes = { "n" },
	},
	{
		key = "<leader><C-h>",
		action = ":wincmd <S-h><CR>",
		modes = { "n" },
	},
	{
		key = "<leader><C-j>",
		action = ":wincmd <S-j><CR>",
		modes = { "n" },
	},
	{
		key = "<leader><C-k>",
		action = ":wincmd <S-k><CR>",
		modes = { "n" },
	},
	-- Splits sizing
	{
		key = "<A-=>",
		action = ':exe "resize " . (winheight(0) * 6/1)<CR>',
		modes = { "n" },
	},
	{
		key = "<A-->",
		action = ':exe "resize " . (winheight(0) * 1/6)<CR>',
		modes = { "n" },
	},
	{
		key = "<A-a>",
		action = '<C-w>10<',
		modes = { "n" },
	},
	{
		key = "<A-q>",
		action = '<C-w>10>',
		modes = { "n" },
	},
	{
		desc = "Clear last incsearch highlight",
		key = "<CR>",
		action = ":noh<CR>",
		modes = { "n" }
	},
	-- Telescope
	-- <C-?> to get help inside opened telescope
	{
		desc   = "Telescope find files",
		key    = "<C-p>",
		action = ":Telescope find_files<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope live grep",
		key    = "<leader>fg",
		action = ":Telescope live_grep<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope buffers",
		key    = "<leader>fb",
		action = ":Telescope buffers<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope help tags",
		key    = "<leader>fh",
		action = ":Telescope help_tags<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope neoclip",
		key    = "<leader>n",
		action = ":Telescope neoclip<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope projects",
		key    = "<leader>fp",
		action = ":Telescope projects<CR>",
		modes  = { "n" }
	},
	{
		desc   = "Telescope oldfiles",
		key    = "<leader>fr",
		action = ":Telescope oldfiles<CR>",
		modes  = { "n" }
	},
	-- Move current line / block with Alt-j/k ala vscode
	{
		desc = "Move current line up",
		key = "<A-k>",
		action = "<Esc>:m .-2<CR>==g",
		modes = { "n", "i" },
	},
	{
		desc = "Move current line down",
		key = "<A-j>",
		action = "<Esc>:m .+1<CR>==g",
		modes = { "n", "i" },
	},

	-- Switch between opened buffers
	{
		desc = "Next opened buffer",
		key = "<S-l>",
		action = ":BufferLineCycleNext<CR>",
		modes = { "n" },
	},
	{
		desc = "Previous opened buffer",
		key = "<S-h>",
		action = ":BufferLineCyclePrev<CR>",
		modes = { "n" },
	},
	{
		desc = "Buffer pick",
		key = "<leader>bp",
		action = "<cmd>BufferLinePick<CR>",
		modes = { "n" }
	},
	{
		key = "<C-_>",
		action = '<ESC><CMD>lua require("Comment.api").toggle.linewise(vim.fn.visualmode())<CR>',
		modes = { "x" } -- ? idk
	},
	{
		desc = "Save a file",
		key = "<C-s>",
		action = ":w<CR>",
		modes = { "n" },
	},
	{
		key = "<C-s>",
		action = "<esc>:w<CR>i",
		modes = { "i" },
	},
	{
		desc = "Toggle trouble menu",
		key = "<leader>xx",
		action = "<cmd>TroubleToggle<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Toggle trouble menu for workspace",
		key = "<leader>xw",
		action = "<cmd>TroubleToggle workspace_diagnostics<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Toggle trouble menu for document",
		key = "<leader>xd",
		action = "<cmd>TroubleToggle document_diagnostics<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Toggle trouble loclist",
		key = "<leader>xl",
		action = "<cmd>TroubleToggle loclist<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Toggle trouble quickfix",
		key = "<leader>xq",
		action = "<cmd>TroubleToggle quickfix<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Toggle trouble lsp references",
		key = "<leader>gR",
		action = "<cmd>Trouble lsp_references<cr>",
		modes = { "n" },
		opts = { silent = true },
	},
	{
		desc = "Restore the session for the current directory",
		key = "<leader>qs",
		action = function()
			return require('persistence').load()
		end,
		modes = { "n" },
	},
	{
		desc = "Restore the last session",
		key = "<leader>ql",
		action = function()
			return require('persistence').load({ last = true })
		end,
		modes = { "n" },
	},
	{
		desc = "Stop persistence",
		key = "<leader>qd",
		action = function()
			return require('persistence').stop()
		end,
		modes = { "n" },
	},
	-- Terminal splits navigation
	{
		key = "<C-h>",
		action = "<C-\\><C-N><C-w>h",
		modes = { "t" },
	},
	{
		key = "<C-j>",
		action = "<C-\\><C-N><C-w>j",
		modes = { "t" },
	},
	{
		key = "<C-k>",
		action = "<C-\\><C-N><C-w>k",
		modes = { "t" },
	},
	{
		key = "<C-l>",
		action = "<C-\\><C-N><C-w>l",
		modes = { "t" },
	},
	-- Navigate tab completion with <c-j> and <c-k>
	-- runs conditionally
	{
		key = "<C-j>",
		action = 'pumvisible() ? "\\<C-n>" : "\\<C-j>"',
		modes = { "c" },
		opts = { expr = true }
	},
	{
		key = "<C-k>",
		action = 'pumvisible() ? "\\<C-p>" : "\\<C-k>"',
		modes = { "c" },
		opts = { expr = true }
	},
	{
		desc = "Go to file selection as a horizontal split in telescope",
		key = "<C-x>",
	},
	{
		desc = "Go to file selection as a vertical split in telescope",
		key = "<C-v>",
	},
	{
		desc = "Go to file in a new tab",
		key = "<C-t>",
	},
	{
		desc = "Close telescope",
		key = "<C-c>",
	},
	{
		desc = "Go to next occurence of current word",
		key = "<C-d>",
		action = '<cmd>lua require"illuminate".next_reference{wrap=true}<cr>',
		modes = { "n" },
	},
	{
		desc = "Go to next occurence of current word",
		key = "<C-A-d>",
		action = '<cmd>lua require"illuminate".next_reference{reverse=true,wrap=true}<cr>',
		modes = { "n" }
	}
	-- {
	-- 	desc = "Help menu",
	-- 	key = "<leader>hk",
	-- 	action = ":Legendary<CR>",
	-- 	modes = { "n" },
	-- 	set_with_nvim_set_keymap = true,
	-- },
}

-----------------------------------------
-- Here starts plugin specific keymaps --
-----------------------------------------

M.toggleterm = {
	{
		desc = "Open lazygit in current directory",
		key = "<leader>gg",
		action = function()
			return utils.exec_in_term('lazygit .', 'float', true)
		end,
		modes = { "n" },
	},
}

M.legendary = {
	{
		desc = "Help menu",
		key = "<leader>hk",
		action = ":Legendary keymaps<CR>",
		modes = { "n" },
		set_with_nvim_set_keymap = true,
	},
}


M.comment = {
	-- Easier comments
	{
		desc = "Linewise toggle current line",
		key = "<C-_>",
		action = function()
			return require("Comment.api").toggle.linewise.current()
		end,
		modes = { "n" }
	},
	{
		desc = "Blockwise toggle current line using C-/",
		key = "<C-\\>",
		action = function()
			return require("Comment.api").toggle.blockwise.current()
		end,
		modes = { "n" }
	},
	{
		-- Example: <leader>gc3j will comment 4 lines
		desc = "Linewise toggle multiple line with dot-repeat support",
		key = "<leader>gc",
		action = '<CMD>lua require("Comment.api").call("toggle.linewise")<CR>g@',
		modes = { "n" }
	},
	{
		-- Example: <leader>gb3j will comment 4 lines
		desc = "Linewise toggle multiple line with dot-repeat support",
		key = "<leader>gb",
		action = '<CMD>lua require("Comment.api").call("toggle.blockwise")<CR>g@',
		modes = { "n" }
	},
	--comment.nvim for visual mode
	{
		key = "<C-_>",
		action = '<ESC><CMD>lua require("Comment.api").toggle.linewise(vim.fn.visualmode())<CR>',
		modes = { "x" } -- ? idk
	},
}

M.hlslens = {
	{
		key = 'n',
		action = [[<Cmd>execute('normal! ' . v:count1 . 'n')<CR><Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { "n" }

	},
	{
		key = 'N',
		action = [[<Cmd>execute('normal! ' . v:count1 . 'N')<CR><Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { "n" }
	},
	{
		key = "*",
		action = [[*<Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { 'n' }
	},
	{
		key = "#",
		action = [[#<Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { "n" }
	},
	{
		key = "g*",
		action = [[g*<Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { "n" }
	},
	{
		key = "g#",
		action = [[g#<Cmd>lua require('hlslens').start()<CR>zz]],
		modes = { "n" }
	}

}




return M







-- <A-c>/c	create	Create file/folder at current path (trailing path separator creates folder)
-- <S-CR>	  create_from_prompt	Create and open file/folder from prompt (trailing path separator creates folder)
-- <A-r>/r	rename	Rename multi-selected files/folders
-- <A-m>/m	move	Move multi-selected files/folders to current path
-- <A-y>/y	copy	Copy (multi-)selected files/folders to current path
-- <A-d>/d	remove	Delete (multi-)selected files/folders
-- <C-o>/o	open	Open file/folder with default system application
-- <C-g>/g	goto_parent_dir	Go to parent directory
-- <C-e>/e	goto_home_dir	Go to home directory
-- <C-w>/w	goto_cwd	Go to current working directory (cwd)
-- <C-t>/t	change_cwd	Change nvim's cwd to selected folder/file(parent)
-- <C-f>/f	toggle_browser	Toggle between file and folder browser
-- <C-h>/h	toggle_hidden	Toggle hidden files/folders
-- <C-s>/s	toggle_all	Toggle all entries ignoring ./ and ../
