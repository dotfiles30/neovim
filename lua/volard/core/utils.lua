local M = {}

local status, terminal_plugin = pcall(require, 'toggleterm')
if status then
	M.Terminal = require("toggleterm.terminal").Terminal
	function M.exec_in_term(command, direction, close_on_exit)
		M.Terminal:new({
			cmd = command,
			-- hidden = false,
			direction = direction,
			close_on_exit = close_on_exit,
		}):open()
	end

	function M.TERMTAB_OPEN()
		local term = M.Terminal:new({
			hidden = true,
			direction = "tab"
		})
		term:open()
	end
end
M.api = vim.api

local os = vim.loop.os_uname().sysname

function M.map(mode, lhs, rhs, opts)
	local options = { noremap = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

function M.get_plugins_keys(plugin)
	local keys = {}
	for _, conf in ipairs(plugin) do
		table.insert(keys, conf.key)
	end
	return keys
end

---Pretty print lua table
function _G.dump(...)
	local objects = vim.tbl_map(vim.inspect, { ... })
	print(unpack(objects))
end

function M.find_cpp_reference(request)
	request = "https://cppreference.com/" .. request
	--request = "https://cppreference.com/"
	print(request)
	-- os.execute("start https://google.com")
	vim.cmd [[exec "start https://google.com"]]

	if package.config:sub(1, 1) == '\\' then -- windows
		-- print(string.format('start "%s"', request))
		-- vim.cmd(string.format('start "%s"', request))
		--vim.jobstart('start', request)
		--os.execute(string.format('start "%s"', request))
		-- the only systems left should understand uname...
	elseif (io.popen("uname -s"):read '*a') == "Darwin" then -- OSX/Darwin ? (I can not test.)
		os.execute(string.format('open "%s"', request))
	else -- that ought to only leave Linux
		-- should work on X-based distros.
		os.execute(string.format('xdg-open "%s"', request))
	end
end

function M.keymap_setup(keymap)
	for _, conf in ipairs(keymap) do
		if conf.key and conf.action and conf.modes then -- If this key shold be mapped

			-- If this key shold be mapped with vim.api.nvim_set_keymap()
			if conf.set_with_nvim_set_keymap then
				for _, mode in ipairs(conf.modes) do
					if conf.opts then
						M.map(mode, conf.key, conf.action, conf.opts)
					else
						M.map(mode, conf.key, conf.action)
					end
				end

			else
				if conf.opts then
					vim.keymap.set(conf.modes, conf.key, conf.action, conf.opts)
				else
					vim.keymap.set(conf.modes, conf.key, conf.action)
				end
			end
		end
	end
end

return M
