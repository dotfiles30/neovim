-- If I use vim.env.HOME i got smth like invalid command Cusersuser...so I hardcoded again
-- vim.o.shell            = "J:/Users/volard/scoop/shims/pwsh.exe"

-- My globals --
vim.g.Utils  = require('volard.core.utils')
vim.g.Keymap = require('volard.core.keymaps')


vim.cmd [[set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz]]
-- for workman layout (thanks to the https://nic-west.com/posts/workman-layout/)
vim.cmd [[set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz,qq,dw,re,wr,bt,jy,fu,ui,po,\\;p,aa,ss,hd,tf,gg,yh,nj,ek,ol,i\\;,zz,xx,mc,cv,vb,kn,lm,QQ,DW,RE,WR,BT,JY,FU,UI,PO,:P,AA,SS,HD,TF,GG,YH,NJ,EK,OL,I:,ZZ,XX,MC,CV,VB,KN,LM]]

vim.opt.cmdheight     = 6 -- Better display for messages
vim.o.relativenumber  = true -- relative numbers
-- vim.opt.helplang      = "ru" -- NOTE Dont forget to setup ruvim package https://neovim.io/doc/user/russian.html
vim.opt.colorcolumn   = "90" -- set gutter to split 90 chars
vim.opt.laststatus    = 3 -- set global line status
vim.o.smartindent     = true -- intellegent indenting
vim.o.smartcase       = true
vim.o.incsearch       = true -- cycle search results
vim.o.hidden          = true -- Idk))
vim.o.cin             = true -- better c++ or smth
vim.o.mousehide       = true -- Hide mouse pointer on typing
vim.o.autoindent      = true -- remember previous indenting
vim.g.mapleader       = ' ' -- Map leader key
vim.o.autowrite       = true -- it should works like onfokuslost but it doesn't
vim.o.autowriteall    = true -- Idk))
vim.opt.backup        = false -- creates a backup file
vim.opt.clipboard     = "unnamedplus" -- allows neovim to access the system clipboard
vim.opt.cmdheight     = 1 -- more space in the neovim command line for displaying messages
vim.opt.conceallevel  = 0 -- so that `` is visible in markdown files
vim.opt.fileencoding  = "utf-8" -- the encoding written to a file
vim.opt.hlsearch      = true -- highlight all matches on previous search pattern
vim.opt.ignorecase    = false -- ignore case in search patterns
vim.opt.mouse         = "a" -- allow the mouse to be used in neovim
vim.opt.smartcase     = true -- smart case
vim.opt.smartindent   = true -- make indenting smarter again
vim.opt.splitbelow    = true -- force all horizontal splits to go below current window
vim.opt.splitright    = true -- force all vertical splits to go to the right of current window
vim.opt.swapfile      = false -- creates a swapfile
vim.opt.timeoutlen    = 800 -- time to wait for a mapped sequence to complete (in milliseconds) 1000 is default
vim.opt.undofile      = true -- enable persistent undo
vim.opt.updatetime    = 50 -- faster completion (4000ms default)
vim.opt.writebackup   = false -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
vim.opt.expandtab     = false -- convert tabs to spaces
vim.opt.shiftwidth    = 4 -- the number of spaces inserted for each indentation
vim.opt.tabstop       = 4 -- insert 4 spaces for a tab
vim.opt.cursorline    = true -- highlight the current line
vim.opt.number        = true -- set numbered lines
vim.opt.numberwidth   = 4 -- set number column width to 2 {default 4}
vim.opt.signcolumn    = "yes" -- always show the sign column, otherwise it would shift the text each time
vim.opt.wrap          = false -- display lines as one long line
vim.opt.scrolloff     = 5 -- start vertical scroll earlier
vim.opt.sidescrolloff = 5 -- start horizontal scroll earlier
vim.o.lazyredraw      = true -- useful for when executing macros.
vim.go.updatetime     = 100 -- You will have bad experience for diagnostic messages when it's default 4000.
vim.o.autoread        = true -- automatic reload file on buffer changed outside of vim
-- vim.o.wildmenu         = true                  -- Decent wildmenu for command prompt
-- vim.o.wop = 'fuzzy'
-- vim.o.wildmode         = 'list:longest'
-- vim.o.wildignore       = '.hg,.svn,*~,*.png,*.jpg,*.gif,*.settings,Thumbs.db,*.min.js,*.swp,publish/*,intermediate/*,*.o,*.hi,Zend,vendor,*/tmp/*,*.so,*.swp,*.zip,*.pyc,*.db,*.sqlite'


--- GUI ---
vim.opt.termguicolors = true -- True color support
-- vim.o.guicursor = 'n-v-c:block-Cursor/lCursor-blinkon0,i-ci:ver25-Cursor/lCursor,r-cr:hor20-Cursor/lCursor'

-- Remove toolbar
-- lua api seems to not be able to access this option
vim.cmd('set guioptions-=T')
vim.o.inccommand = 'nosplit'
vim.go.t_Co = '256'
vim.o.background = 'dark'
vim.g.base16colorspace = 256
vim.o.foldenable = false
-- https://github.com/vim/vim/issues/1735#issuecomment-383353563
-- vim.o.lazyredraw = true
-- No more beeps
vim.o.vb = true
vim.go.t_vb = ''
vim.o.synmaxcol = 500

-- vim.cmd('syntax on')
-- vim.cmd('hi Normal ctermbg=NONE')



--- NEOVIDE ---
if vim.g.neovide then
	vim.g.neovide_no_idle                 = true
	vim.g.neovide_input_use_logo          = true
	vim.g.neovide_cursor_antialiasing     = true
	vim.g.neovide_input_macos_alt_is_meta = true
	vim.g.neovide_cursor_animation_length = 0.02
	vim.g.neovide_cursor_trail_length     = 0.02
	vim.g.neovide_cursor_antialiasing     = true
	vim.g.neovide_remember_window_size    = true
	vim.cmd([[set guifont=JetBrainsMono\ Nerd\ Font:h13]])
end
