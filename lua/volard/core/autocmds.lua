-- Thanks to https://github.com/brainfucksec/neovim-lua

-- Define autocommands with Lua APIs
-- See: h:api-autocmd, h:augroup

local augroup = vim.api.nvim_create_augroup -- Create/get autocommand group
local autocmd = vim.api.nvim_create_autocmd -- Create autocommand

autocmd('VimEnter', {
	callback = function()
		vim.g.plugins_count = #vim.tbl_keys(packer_plugins or {})
		print(vim.g.plugins_count)
	end
})

-- Highlight on yank
augroup('YankHighlight', { clear = true })
autocmd('TextYankPost', {
	group = 'YankHighlight',
	callback = function()
		vim.highlight.on_yank({ higroup = 'IncSearch', timeout = '500' })
	end
})

-- Remove whitespace on save - I use formatter but want to store that option too
-- autocmd('BufWritePre', {
--   pattern = '*',
--   command = ":%s/\\s\\+$//e"
-- })



-- Don't auto commenting new lines
autocmd('BufEnter', {
	pattern = '*',
	command = 'set fo-=c fo-=r fo-=o'
})

-- Set indentation to 2 spaces
augroup('setIndent', { clear = true })
autocmd('Filetype', {
	group = 'setIndent',
	pattern = { 'xml', 'html', 'xhtml', 'css', 'scss', 'javascript', 'typescript',
		'yaml', 'lua'
	},
	command = 'setlocal shiftwidth=2 tabstop=2 autoindent smartindent'
})

-- Terminal settings:
-- Open a Terminal on the right tab
autocmd('CmdlineEnter', {
	command = 'command! Term :botright vsplit term://$SHELL'
})

-- Open file finder on opening nvim
-- local group = vim.api.nvim_create_augroup("Volard's autogroup", {clear = true})
-- vim.api.nvim_create_autocmd("VimEnter", {command = "Telescope find_files",
-- 	group = group, once = true})

-- Settings for filetypes:
-- Disable line length marker
-- augroup('setLineLength', { clear = true })
-- autocmd('Filetype', {
--   group = 'setLineLength',
--   pattern = { 'text', 'markdown', 'html', 'xhtml', 'javascript', 'typescript' },
--   command = 'setlocal cc=0'
-- })


-- Enter insert mode when switching to terminal
-- autocmd('TermOpen', {
--   command = 'setlocal listchars= nonumber norelativenumber nocursorline',
-- })

-- autocmd('TermOpen', {
--   pattern = '*',
--   command = 'startinsert'
-- })

-- Close terminal buffer on process exit
-- autocmd('BufLeave', {
--   pattern = 'term://*',
--   command = 'stopinsert'
-- })
