-- local home = os.getenv('HOME')
local home = vim.env.HOME
local db = require('dashboard')
db.preview_file_height = 12
db.preview_file_width = 80
db.custom_center = {

	-- button("e", "  New file", "<cmd>ene <CR>"),
	-- button("SPC f f", "  Find file"),
	-- button("SPC f h", "  Recently opened files"),
	-- button("SPC f r", "  Frecency/MRU"),
	-- button("SPC f g", "  Find word"),
	-- button("SPC f m", "  Jump to bookmarks"),
	-- button("SPC s l", "  Open last session"),


	{
		icon = '  ',
		desc = 'Create new file                         ',
		shortcut = 'Ctrl n',
		action = 'new'
	},

	{
		icon = '  ',
		desc = 'Recently latest session                 ',
		shortcut = 'SPC sl',
		action = 'SessionLoad'
	},

	{
		icon = '  ',
		desc = 'Recently opened files                   ',
		action = 'DashboardFindHistory',
		shortcut = 'SPC fh'
	},

	{
		icon = '  ',
		desc = 'Find  File                              ',
		action = 'Telescope find_files find_command=rg,--hidden,--files',
		shortcut = 'SPC ff'
	},

	{
		icon = '  ',
		desc = 'File Browser                            ',
		action = 'Telescope file_browser',
		shortcut = 'SPC fb'
	},

	{
		icon = '  ',
		desc = 'Find  word                              ',
		action = 'Telescope live_grep',
		shortcut = 'SPC fw'
	}
}
