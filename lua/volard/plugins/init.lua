local status_impatient, impatient = pcall(require, 'impatient')
if (status_impatient) then
	impatient.enable_profile()
end

require('volard.plugins.xkbswitch').setup()
-- require('volard.plugins.alpha').setup()
require("auto-save").setup{}
require('volard.plugins.bufferline').setup()
require('volard.plugins.treesitter').setup()
require('volard.plugins.illuminate').setup()
require('volard.plugins.neoclip').setup()
require('volard.plugins.telescope')
