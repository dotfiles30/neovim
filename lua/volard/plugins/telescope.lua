local status, telescope = pcall(require, "telescope")
if (not status) then
	vim.notify("Telescope is not installed", vim.log.levels.ERROR)
	return
end

local options = {}


--local status_file_browser, file_browser = require("telescope-file-browser.nvim")
--if (status_file_browser) then
options.extensions = {
	file_browser = {
		-- theme = "ivy",
		-- disables netrw and use telescope-file-browser in its place
		hijack_netrw = true,
		-- mappings = {
		-- 	["i"] = {
		-- your custom insert mode mappings
		-- },
		-- ["n"] = {
		-- your custom normal mode mappings
		-- },
		-- },
	},
}
--end

telescope.setup { options }


telescope.load_extension("file_browser")

local status_project, _ = require("project_nvim")
if (status_project) then
	telescope.load_extension('projects')
end


local status_neoclip, _ = require("neoclip")
if (status_neoclip) then
	telescope.load_extension('neoclip')
end
