local status, formatter = pcall(require, "formatter")
if (not status) then return end


-- Utilities for creating configurations
local util = formatter.util
-- Provides the Format and FormatWrite commands
formatter.setup({
	-- Enable or disable logging
	logging = false,
	-- Set the log level
	-- log_level = vim.log.levels.WARN,
	-- All formatter configurations are opt-in
	filetype = {
		-- Formatter configurations for filetype "lua" go here
		-- and will be executed in order
		lua = {
			-- "formatter.filetypes.lua" defines default configurations for the
			-- "lua" filetype
			require("formatter.filetypes.lua").stylua,

			-- You can also define your own configuration
			-- function()
			--   -- Supports conditional formatting
			--   if util.get_current_buffer_file_name() == "special.lua" then
			--     return nil
			--   end
			--
			--   -- Full specification of configurations is down below and in Vim help
			--   -- files
			--   return {
			--     exe = "uncrustify",
			--     args = {
			--       util.escape_path(util.get_current_buffer_file_path()),
			--       "-c -",
			--     },
			--     stdin = true,
			--   }
			-- end
		},
		cs = {
			require("formatter.filetypes.cs").dotnetformat,
			-- 	function()
			-- 	  -- Supports conditional formatting
			-- 	  if util.get_current_buffer_file_name() == "special.lua" then
			-- 	    return nil
			-- 	  end
			--
			-- 	  -- Full specification of configurations is down below and in Vim help
			-- 	  -- files
			-- 	  return {
			-- 	    exe = "uncrustify",
			-- 	    args = {
			-- 	      util.escape_path(util.get_current_buffer_file_path()),
			-- 	      "-c -",
			-- 		  "-q"
			-- 	    },
			-- 	    stdin = true,
			-- 	  }
			-- 	end
			-- },
			--
			-- Use the special "*" filetype for defining formatter configurations on
			-- any filetype
			-- ["*"] = {
			-- 	-- "formatter.filetypes.any" defines default configurations for any
			-- 	-- filetype
			-- 	require("formatter.filetypes.any").remove_trailing_whitespace,
		},
	},
})
