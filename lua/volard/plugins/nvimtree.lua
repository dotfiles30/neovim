local M = {}


function M.NvimTreeTrash()
	local function get_user_input_char()
		local c = vim.fn.getchar()
		return vim.fn.nr2char(c)
	end

	local function clear_prompt()
		vim.api.nvim_command('normal :esc<CR>')
	end

	local function remove()
		local lib = require('nvim-tree.lib')

		local vars = require('volard.core.vars')
		local trash_path = vars.trash_path

		local node = lib.get_node_at_cursor()
		if node then
			print(node.absolute_path)
			print(node.name)
			os.rename(node.absolute_path, trash_path .. node.name)
			print('mi "' .. node.absolute_path .. '" "' .. trash_path .. node.name .. '"')
			os.execute("move-item '" .. node.absolute_path .. "' '" .. trash_path .. node.name .. "'")
			lib.refresh_tree()
		end
	end

	print("Are you sure about that shit? y/n")
	local ans = get_user_input_char()

	if ans:match('^y') then
		remove()
	end

	-- clear_prompt()
end

function M.setup()

	local status, nvim_tree = pcall(require, "nvim-tree")
	if (not status) then
		vim.notify("NvimTree is not installed", vim.log.levels.ERROR)
		return
	end

	nvim_tree.setup({
		sync_root_with_cwd = true,
		respect_buf_cwd = true,
		auto_reload_on_write = true,
		hijack_cursor = true,
		open_on_setup = false,
		hijack_unnamed_buffer_when_opening = false,
		ignore_ft_on_setup = { "alpha" },
		reload_on_bufenter = true,
		-- trash = { -- good option but on linux TODO add check for linux or use function
		-- 	cmd = trash,
		-- },
		view = {
			adaptive_size = true,
			hide_root_folder = true,
			width = 30,
			mappings = {
				list = {
					{ key = "u", action = "dir_up" },
					{ key = "d", cb = ":lua NvimTreeTrash()<CR>" }
				}
			}
		},
		renderer = {
			indent_markers = {
				enable = false
			},
			highlight_git = true,
			icons = {
				show = {
					git = true,
				}
			}
		},
		update_focused_file = {
			enable = true,
			update_root = true,
		},
		filters = {
			dotfiles = true
		},
		actions = {
			remove_file = {
				close_window = true
			},
			open_file = {
				quit_on_open = true,
				resize_window = true,
				window_picker = {
					enable = true,
					chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890",
					exclude = {
						filetype = { "notify", "packer", "qf", "diff", "fugitive", "fugitiveblame" },
						buftype = { "nofile", "terminal", "help" },
					}
				}
			}
		},
		git = {
			enable = true,
			ignore = false,
		}
	})

end

return M
