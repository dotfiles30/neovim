local M = {}

function M.setup()
	local status_ok, tsconfigs = pcall(require, "nvim-treesitter.configs")
	if not status_ok then
		vim.notify("Treesitter is not installed", vim.log.levels.ERROR)
		return
	end

	tsconfigs.setup({
		-- ensure_installed = "all", -- one of "all" or a list of languages
		ensure_installed = {
			"python",
			"lua",
			-- "c_sharp",
			-- "cpp",
			-- "dart",
			-- "html",
			-- "json",
			-- "latex",
			-- "markdown",
			-- "pascal",
			-- "rust",
			-- "regex",
			-- "scss",
			-- "toml",
			-- "vim",
			-- "pug",
			-- "kotlin",
			-- "javascript",
			-- "java",
			-- "go",
			-- "gitignore",
			-- "astro"
		},
		-- ignore_install = { "lua" }, -- List of parsers to ignore installing
		highlight = {
			enable = true, -- false will disable the whole extension
			-- disable = {  }, -- list of language that will be disabled
		},
		autopairs = {
			enable = true,
		},
		indent = {
			enable = true,
			-- disable = { "python", "css" }
		},
	})

	-- :TSUpdate or smth to init/update tree sitter

end

return M
