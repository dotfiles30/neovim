local M = {}

function M.setup()
	vim.g.XkbSwitchEnabled = 1
	vim.g.XkbSwitchLib = require('volard.core.vars').xkbswitch_path
end

return M
