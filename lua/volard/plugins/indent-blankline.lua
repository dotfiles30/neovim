local M = {}


function M.setup()
	vim.opt.list = true
	-- vim.opt.listchars:append("eol:↴")
	vim.opt.listchars:append("space:⋅")

	local status_ok, indent = pcall(require, "indent_blankline")
	if not status_ok then
		vim.notify("Indent-blankline is not installed", vim.log.levels.ERROR)
		return
	end

	indent.setup {
		show_end_of_line = false,
		space_char_blankline = ".",
		show_current_context = true,
		show_current_context_start = true
	}

end

return M
