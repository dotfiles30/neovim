local M = {}

function M.setup()
	local status_ok, legendary = pcall(require, "legendary")
	if not status_ok then
		vim.notify("Legendary is not installed", vim.log.levels.ERROR)
		return
	end


	local mappings = {}
	for _, section in ipairs(vim.g.Keymap) do
		for _, conf in ipairs(section) do
			if (conf.desc) then
				table.insert(mappings, { conf.key, description = conf.desc })
			end
		end
	end


	legendary.setup({
		-- Include builtins by default, set to false to disable
		include_builtin = true,
		-- Include the commands that legendary.nvim creates itself
		-- in the legend by default, set to false to disable
		include_legendary_cmds = true,
		-- Customize the prompt that appears on your vim.ui.select() handler
		-- Can be a string or a function that takes the `kind` and returns
		-- a string. See "Item Kinds" below for details. By default,
		-- prompt is 'Legendary' when searching all items,
		-- 'Legendary Keymaps' when searching keymaps,
		-- 'Legendary Commands' when searching commands,
		-- and 'Legendary Autocmds' when searching autocmds.
		select_prompt = nil,
		-- Optionally pass a custom formatter function. This function
		-- receives the item as a parameter and must return a table of
		-- non-nil string values for display. It must return the same
		-- number of values for each item to work correctly.
		-- The values will be used as column values when formatted.
		-- See function `get_default_format_values(item)` in
		-- `lua/legendary/formatter.lua` to see default implementation.
		formatter = nil,
		-- When you trigger an item via legendary.nvim,
		-- show it at the top next time you use legendary.nvim
		most_recent_item_at_top = true,
		keymaps = mappings,

		commands = {
			{ "LSoutlineToggle", description = "Toggle outline sidebar" }
		},
		autocmds = {},

		-- settings for the :LegendaryScratch command
		scratchpad = {
			-- configure how to show results of evaluated Lua code,
			-- either 'print' or 'float'
			-- Pressing q or <ESC> will close the float
			display_results = 'float',
		},
	})
end

return M
