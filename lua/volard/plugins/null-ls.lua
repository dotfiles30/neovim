local status_ok, nullls = pcall(require, "nullls")
if not status_ok then
	vim.notify("Null-ls is not installed", vim.log.levels.ERROR)
	return
end

nullls.setup({
	sources = {
		nullls.builtins.diagnostics.eslint,
		-- require("null-ls").builtins.completion.spell,
		nullls.builtins.formatting.astyle,
		nullls.builtins.diagnostics.cppcheck,
		nullls.builtins.formatting.dart_format,
	},
})
