local M = {}

function M.setup()
	local status, comment = pcall(require, "Comment")
	if (not status) then
		vim.notify("Comment is not installed", vim.log.levels.ERROR)
		return
	end

	comment.setup(
		{
			---Lines to be ignored while comment/uncomment.
			---Could be a regex string or a function that returns a regex string.
			---Example: Use '^$' to ignore empty lines
			---@type string|fun():string
			ignore = "^$",
		}
	)
end

return M
