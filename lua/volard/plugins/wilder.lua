local M = {}

function M.setup()

	local status, wilder = pcall(require, "wilder")
	if (not status) then
		vim.notify("Wilder is not installed", vim.log.levels.ERROR)
		return
	end


	wilder.setup(
		{
			modes = { ':', '/', '?' },
			next_key = '<C-n>',
			previous_key = '<C-p>',
			-- enable_cmdline_enter = 0
		}
	)

	wilder.set_option('renderer', wilder.popupmenu_renderer({
		highlighter = wilder.basic_highlighter(),
		left = { ' ', wilder.popupmenu_devicons() },
		right = { ' ', wilder.popupmenu_scrollbar() },
	}))


	-- wilder.set_option('renderer', wilder.popupmenu_renderer({
	-- 	wilder.popupmenu_border_theme({
	-- 		highlights = {
				-- border = 'Normal', -- highlight to use for the border
			-- },
			-- 'single', 'double', 'rounded' or 'solid'
			-- can also be a list of 8 characters, see :h wilder#popupmenu_border_theme() for more details
			-- border = 'rounded',
		-- }),


	-- }))

	-- wilder.set_option('renderer', wilder.renderer_mux({
	-- 	[':'] = wilder.popupmenu_renderer({
	-- 		highlighter = wilder.basic_highlighter(),
	-- 	}),
	-- 	['/'] = wilder.wildmenu_renderer({
	-- 		highlighter = wilder.basic_highlighter(),
	-- 	}),
	-- }))


	-- wilder.set_option('renderer', wilder.popupmenu_renderer(
	--   wilder.popupmenu_palette_theme({
	--     -- 'single', 'double', 'rounded' or 'solid'
	--     -- can also be a list of 8 characters, see :h wilder#popupmenu_palette_theme() for more details
	--     border = 'rounded',
	--     max_height = '75%',      -- max height of the palette
	--     min_height = 0,          -- set to the same as 'max_height' for a fixed height window
	--     prompt_position = 'top', -- 'top' or 'bottom' to set the location of the prompt
	--     reverse = 0,             -- set to 1 to reverse the order of the list, use in combination with 'prompt_position'
	--   })
	-- ))
end

return M
