local status, toggleterm = pcall(require, "toggleterm")
if (not status) then
	vim.notify("Toggleterm is not installed", vim.log.levels.ERROR)
	return
end

local vars = require('volard.core.vars')

toggleterm.setup({
	size = 10,
	open_mapping = [[<A-/>]],
	hide_numbers = true,
	shade_terminals = true,
	shading_factor = 2,
	-- hidden = false,
	start_in_insert = true,
	insert_mappings = true,
	-- persist_size = true,
	-- float | verticle | tab
	direction = "horizontal",
	close_on_exit = true,
	-- shell = vim.o.shell,
	shell = vars.shell_path,
	-- 'single' | 'double' | 'shadow' | 'curved' | ... other options supported by win open
	float_opts = {
		border = "single",
	}
})
