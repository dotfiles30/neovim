_ = vim.cmd [[packadd packer.nvim]]
-- vim.api.nvim_cmd({
--   cmd = "packadd",
--   args = { "packer.vim" },
-- }, {})


-- Autoinstall packer

-- local cmd = vim.cmd
-- local status, packer = pcall(require, 'packer')

-- if (not status) then
-- 	local packer_path = vim.fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'

-- 	print('Cloning packer..')
-- 	-- remove the dir before cloning
-- 	vim.fn.delete(packer_path, 'rf')
-- 	vim.fn.system({
-- 		'git',
-- 		'clone',
-- 		'https://github.com/wbthomason/packer.nvim',
-- 		'--depth',
-- 		'20',
-- 		packer_path,
-- 	})

-- 	cmd('packadd packer.nvim')
-- 	status, packer = pcall(require, 'packer')

-- 	if status then
-- 		print('Packer cloned successfully.')
-- 	else
-- 		error("Couldn't clone packer !\nPacker path: " .. packer_path .. '\n' .. packer)
-- 	end
-- end


local has = function(x)
	return vim.fn.has(x) == 1
  end
  
  local executable = function(x)
	return vim.fn.executable(x) == 1
  end
  
  local is_wsl = (function()
	local output = vim.fn.systemlist "uname -r"
	return not not string.find(output[1] or "", "WSL")
  end)()
  
  local is_mac = has "macunix"
  local is_linux = not is_wsl and not is_mac
  
  local max_jobs = nil
  if is_mac then
	max_jobs = 32
  end

	local packer = require('packer')

packer.init({
	git = {
		clone_timeout = 800, -- Timeout, in seconds, for git clones
	},
	-- auto_clean = true,
	compile_on_sync = true,
})

-- Automatically run :PackerCompile whenever packer_init.lua is updated with an autocommand:
vim.api.nvim_create_autocmd('BufWritePost', {
	group = vim.api.nvim_create_augroup('PACKER', { clear = true }),
	pattern = 'packer_init.lua',
	command = 'source <afile> | PackerCompile',
})



-- NOTE opt = true just tells packer.nvim to never automatically load the plugin.
packer.startup({ function()

	local py_use = function(opts)
		if not has "python3" then
		  return
		end
  
		use(opts)
	  end


	use { -- Packer itself
		'wbthomason/packer.nvim',
	}

	use { -- Russian support
		'lyokha/vim-xkbswitch',
	}

	use { -- Startup screen
		'goolord/alpha-nvim',
		event = "VimEnter",
		config = require('volard.plugins.alpha').setup,
		requires = 'kyazdani42/nvim-web-devicons',
	}

	use { -- File sidebar
		'kyazdani42/nvim-tree.lua',
		requires = {
			'kyazdani42/nvim-web-devicons',
			opt = true
		},
		cmd = {
			'NvimTreeClipboard',
			'NvimTreeClose',
			'NvimTreeFindFile',
			'NvimTreeOpen',
			'NvimTreeRefresh',
			'NvimTreeToggle',
		},
		config = require('volard.plugins.nvimtree').setup
	}

	use { -- Auto saver on events
		"Pocco81/auto-save.nvim",
	}

	use { -- Session manager
		"folke/persistence.nvim",
		event = "BufReadPre", -- this will only start session saving when an actual file was opened
		module = "persistence",
		config = function()
			require("persistence").setup()
		end,
	}

	use { -- Keybindins / commands sheets
		'mrjones2014/legendary.nvim',
		cmd = { "Legendary", "Legendary keymaps" },
		config = function()
			require('volard.plugins.legendary_config').setup()
			-- vim.g.Utils.keymap_setup(vim.g.Keymap.legendary)
		end,
		disable = true
	}

	use {
		'lewis6991/impatient.nvim',
		config = function ()
			require'impatient'.enable_profile()
		end,
	}

	use {
		'akinsho/bufferline.nvim',
		tag = "v2.*",
		requires = 'kyazdani42/nvim-web-devicons',
	}


	-- --------------------------
	-- -- TELESCOPE
	-- --------------------------

	use {
		'nvim-telescope/telescope.nvim',
		requires = {
			{
				'nvim-lua/plenary.nvim',
				'kyazdani42/nvim-web-devicons'
			},
		},
		after = { "telescope-file-browser.nvim", "nvim-neoclip.lua" },
	}
	use {
		"nvim-telescope/telescope-file-browser.nvim",
	}
	use {
		'AckslD/nvim-neoclip.lua',
		requires = 'nvim-telescope/telescope.nvim',
	}


	-- --------------------------
	-- -- LSP
	-- --------------------------
	use {
		'neovim/nvim-lspconfig',
	}
	use {
		'glepnir/lspsaga.nvim',
		branch = "main",
		after = 'nvim-lspconfig'
	}
	use {
		'ray-x/lsp_signature.nvim',
		after = 'nvim-lspconfig'
	}
	use { -- language server installer
		'williamboman/nvim-lsp-installer',
	}
	use {
		'folke/lsp-colors.nvim'
	}


	-- --------------------------
	-- -- cmp plugin and its sources
	-- --------------------------
	use({ -- The completion plugin
		{
			'hrsh7th/nvim-cmp',
			event = 'InsertEnter',
			config = require('volard.lsp.cmp').setup,
			requires = {
				'L3MON4D3/LuaSnip',
				event = 'InsertEnter',
				config = require('volard.lsp.luasnip').setup,

				requires = {
					{
						'rafamadriz/friendly-snippets',
						event = 'CursorHold',
					},
				},
			},
		},

		{ -- buffer completions
			'hrsh7th/cmp-buffer',
			after = 'nvim-cmp'
		},

		{ -- path completions
			'hrsh7th/cmp-path',
			after = 'nvim-cmp'
		},

		{ -- cmdline completions
			'hrsh7th/cmp-cmdline',
			after = 'nvim-cmp'
		},

		{ -- lsp completion
			'hrsh7th/cmp-nvim-lsp',
			after = 'nvim-cmp'
		},

		{ -- emoji completion
			'hrsh7th/cmp-emoji',
			after = 'nvim-cmp'
		},

		{ -- luasnip completion
			'saadparwaiz1/cmp_luasnip',
			after = 'nvim-cmp'
		}
	})



	-- --------------------------
	-- -- Utils for coding better experience
	-- --------------------------
	use {
		'numToStr/Comment.nvim',
		keys = (function()
			vim.g.Utils.get_plugins_keys(vim.g.Keymap.comment)
		end)(),
		config = function()
			require('volard.plugins.comment').setup()
			vim.g.Utils.keymap_setup(vim.g.Keymap.comment)
		end,
	}

	-- if is_linux then
	-- 	use "yamatsum/nvim-web-nonicons"
	--   end

	-- Better profiling output for startup.
    use {
		"dstein64/vim-startuptime",
		cmd = "StartupTime",
	  }

	use {
		'folke/trouble.nvim',
		-- cmd = "Trouble", // TODO
		-- config = function()
		-- 	-- Can use P to toggle auto movement
		-- 	require("trouble").setup {
		-- 	  auto_preview = false,
		-- 	  auto_fold = true,
		-- 	}
		--   end,
		requires = "kyazdani42/nvim-web-devicons"
	}
	use {
		'nvim-treesitter/nvim-treesitter',
		run = ':TSUpdate', -- make sure gcc or smth installed
	}
	use {
		'RRethy/vim-illuminate',
	}
	use {
		'akinsho/toggleterm.nvim',
		tag = 'v2.*',
	}
	use {
		'ThePrimeagen/harpoon',
		requires = 'nvim-lua/plenary.nvim',
		disable = true
	}
	use {
		'kdheepak/lazygit.nvim',
		disable = true
	}
	use {
		'jiangmiao/auto-pairs',
		event = 'InsertCharPre',
		after = 'nvim-cmp',
	}
	use {
		'kylechui/nvim-surround',
		disable = true
	}
	use {
		'lukas-reineke/indent-blankline.nvim',
		event = 'BufRead',
		config = require('volard.plugins.indent-blankline').setup
	}

	use {
		'jose-elias-alvarez/null-ls.nvim',
		requires = "nvim-lua/plenary.nvim"
	}

	-- --------------------------
	-- -- Per language/coding type purpose specific stuff
	-- --------------------------
	use {
		'akinsho/flutter-tools.nvim',
		ft = { "flutter", "dart" },
		requires = { 'nvim-lua/plenary.nvim' },
		config = function ()
			require('volard.plugins.flutter-tools')
		end

	}
	use {
		'xeluxee/competitest.nvim',
		requires = 'MunifTanjim/nui.nvim',
		ft = "cpp",
		config = function ()
			require('volard.plugins.competitest')
		end
	}
	use { -- Markdown support
		"iamcco/markdown-preview.nvim",
		run = function() vim.fn["mkdp#util#install"]() end,
		ft = {'md', "markdown"},
		cmd = { 'MarkdownPreview', 'MarkdownPreviewStop' },
		config = function ()
			require('markdown-preview')
		end,
	}


	-- --------------------------
	-- -- Colorshemes
	-- --------------------------
	use {
		'navarasu/onedark.nvim',
		disable = true
	}
	use {
		'folke/tokyonight.nvim',
		disable = true
	}
	use {
		'marko-cerovac/material.nvim',
		config = require('volard.colorshemes.material').setup
	}


	-- --------------------------
	-- -- Other styling
	-- --------------------------
	use {
		'stevearc/dressing.nvim',
		config = function() require('dressing') end
	}

	use {
		'rcarriga/nvim-notify',
		config = function() require('volard.plugins.notify') end
	}

	use {
		'nvim-lualine/lualine.nvim',
		requires = {
			'kyazdani42/nvim-web-devicons',
			opt = true
		},
		event = 'BufEnter',
		after = 'material.nvim',
		config = require('volard.plugins.lualine').setup
	}
	use {
		'karb94/neoscroll.nvim',
		disable = true
	}



	-- --------------------------
	-- -- Misc
	-- --------------------------
	use {
		'lewis6991/gitsigns.nvim',
		disable = true,
		event = 'BufRead',
	}

	use {
		'gelguy/wilder.nvim',
		requires = {
			'kyazdani42/nvim-web-devicons',
			opt = true
		},
		disable = false,
		config = require('volard.plugins.wilder').setup
	}

	use {
		'kevinhwang91/nvim-hlslens',
		event = 'BufRead',
	}

	use { -- TODO whats this
		'mbbill/undotree',
		disable = true
	}

	use { -- TODO whats this
		'phaazon/hop.nvim',
		branch = 'v1', -- optional but strongly recommended
		config = function()
			-- :h hop-config
			require 'hop'.setup { keys = 'etovxqpdygfblzhckisuran' }
		end,
		disable = true,
	}

	use {
		'folke/which-key.nvim',
		disable = true
	}

	use { -- TODO is it sure that I need it ?
		'rhysd/clever-f.vim'
	}

	use { -- Discord rich presence
		'andweeb/presence.nvim',
		disable = true
	}

	use { -- Projects feature
		"ahmedkhalf/project.nvim",
		config = require('volard.plugins.project').setup
	}

	use {
		"folke/todo-comments.nvim",
		requires = "nvim-lua/plenary.nvim",
		config = require('volard.plugins.todo-comments').setup,
		cmd = {
			"TodoQuickFix",
			"TodoTrouble",
			"TodoTelescope"
		},
		after = {
			'trouble.nvim',
			'telescope.nvim'
		}
	}

end,
	config = {
		display = {
			open_fn = function()
				return require('packer.util').float({ border = 'single' })
			end
		},
		profile = {
			enable = true,
			threshold = 1 -- the amount in ms that a plugins load time must be over for it to be included in the profile

		}
	}

})
