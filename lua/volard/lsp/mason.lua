-- local status_ok, mason = pcall(require, "mason")
-- if not status_ok then
-- 	vim.notify("Mason haven't installed lol", vim.log.levels.ERROR)
-- 	return
-- end

-- local icons =  {
--     server_installed = "✓",
--     server_pending = "➜",
--     server_uninstalled = "✗",
-- }

-- mason.setup {
--   ui = {
--     icons = {
--       package_installed = icons.server_installed,
--       package_pending = icons.server_pending,
--       package_uninstalled = icons.server_uninstalled,
--     },
--   },
-- }


local status, mason = pcall(require, "mason")
if (not status) then return end
local status2, lspconfig = pcall(require, "mason-lspconfig")
if (not status2) then return end

mason.setup({

})

lspconfig.setup {
  ensure_installed = { "sumneko_lua", "tailwindcss" },
}
