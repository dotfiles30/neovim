require('volard.lsp.cmp')
require('volard.lsp.lspsaga')
local icons = require('volard.core.icons')

local function setup_icons()
	local kinds = vim.lsp.protocol.CompletionItemKind
	for i, kind in ipairs(kinds) do
		kinds[i] = icons[kind] or kind
	end
end

setup_icons()

vim.diagnostic.config({
	signs = true,
	update_in_insert = true,
	virtual_text = false,
	severity_sort = true,
	float = {
		focusable = false,
		style = "minimal",
		border = "rounded",
		source = "always",
		header = "",
		prefix = "",
	}
})

for type, icon in pairs(icons.diagnostics) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end


vim.lsp.handlers["textDocument/hover"] = vim.lsp.with(vim.lsp.handlers.hover, {
	border = "rounded",
	width = 60,
})

vim.lsp.handlers["textDocument/signatureHelp"] = vim.lsp.with(vim.lsp.handlers.signature_help, {
	border = "rounded",
	width = 60,
})

require('volard.lsp.signature')
require('volard.lsp.lsp-installer')

-- This mason plugin works weird just because curl tool works weird just because there are
-- some issues with ssl authentication or smth but with saying this that's pretty cool so
-- I'll keep it

-- require('volard.lsp.mason')
-- require('volard.lsp.mason-tool-installer')
-- require('volard.lsp.mason-lsp-config')
