local M = {}


function M.setup()

	local status, luasnip = pcall(require, "luasnip")
	if not status then
		vim.notify("Luasnip is not installed", vim.log.levels.ERROR)
		return
	end

	-- integrate vs code like snippets using friendly snippets
	require("luasnip/loaders/from_vscode").lazy_load()
end

return M
