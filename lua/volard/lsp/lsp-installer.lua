local status_ok, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status_ok then
	vim.notify("Lsp installer haven't installed lol", vim.log.levels.ERROR)
	return
end

-- Available commands for lsp-install
-- :LspInstallInfo - opens a graphical overview of your language servers
-- :LspInstall [--sync] [server] ... - installs/reinstalls language servers. Runs in a blocking fashion if the --sync argument is passed (only recommended for scripting purposes).
-- :LspUninstall [--sync] <server> ... - uninstalls language servers. Runs in a blocking fashion if the --sync argument is passed (only recommended for scripting purposes).
-- :LspUninstallAll [--no-confirm] - uninstalls all language servers
-- :LspInstallLog - opens the log file in a new tab window
-- :LspPrintInstalled - prints all installed language servers

local lspconfig = require("lspconfig")

local servers = {
	"omnisharp",
	"sumneko_lua",
	"clangd"
}

lsp_installer.setup({
	ensure_installed = servers,

	ui = {
	 -- Whether to automatically check for outdated servers when opening the UI window.
        check_outdated_servers_on_open = true,
        icons = {
            server_installed = "",
            server_pending = "勒",
            server_uninstalled = ""
        }
    },

	keymaps = {
		-- Keymap to expand a server in the UI
		toggle_server_expand = "<CR>",
		-- Keymap to install the server under the current cursor position
		install_server = "i",
		-- Keymap to reinstall/update the server under the current cursor position
		update_server = "u",
		-- Keymap to check for new version for the server under the current cursor position
		check_server_version = "c",
		-- Keymap to update all installed servers
		update_all_servers = "U",
		-- Keymap to check which installed servers are outdated
		check_outdated_servers = "C",
		-- Keymap to uninstall a server
		uninstall_server = "X",
        },
})

for _, server in pairs(servers) do
	local opts = {
		on_attach = require("volard.lsp.handlers").on_attach,
		capabilities = require("volard.lsp.handlers").capabilities,
		-- flags = {}
	}
	local has_custom_opts, server_custom_opts = pcall(require, "volard.lsp.settings." .. server)
	if has_custom_opts then
		opts = vim.tbl_deep_extend("force", opts, server_custom_opts)
	end
	lspconfig[server].setup(opts)
end
