local status_ok, mason_lsp_config = pcall(require, "mason-lspconfig")
if not status_ok then
	vim.notify("Mason haven't installed lol", vim.log.levels.ERROR)
	return
end


local nvim_lsp = require "lspconfig"
nvim_lsp.tailwindcss.setup {}
nvim_lsp.sumneko_lua.setup {}

-- local servers = {
--   gopls = {},
--   html = {},
--    pyright = {
--     analysis = {
--       typeCheckingMode = "off",
--     },
--   },
--   -- pylsp = {}, -- Integration with rope for refactoring - https://github.com/python-rope/pylsp-rope
--   rust_analyzer = {
--     settings = {
--       ["rust-analyzer"] = {
--         cargo = { allFeatures = true },
--         checkOnSave = {
--           command = "clippy",
--           extraArgs = { "--no-deps" },
--         },
--       },
--     },
--   },
--   sumneko_lua = {
--     settings = {
--       Lua = {
--         runtime = {
--           -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
--           version = "LuaJIT",
--           -- Setup your lua path
--           path = vim.split(package.path, ";"),
--         },
--         diagnostics = {
--           -- Get the language server to recognize the `vim` global
--           globals = { "vim", "describe", "it", "before_each", "after_each", "packer_plugins" },
--           -- disable = { "lowercase-global", "undefined-global", "unused-local", "unused-vararg", "trailing-space" },
--         },
--         workspace = {
--           -- Make the server aware of Neovim runtime files
--           library = {
--             [vim.fn.expand "$VIMRUNTIME/lua"] = true,
--             [vim.fn.expand "$VIMRUNTIME/lua/vim/lsp"] = true,
--           },
--           -- library = vim.api.nvim_get_runtime_file("", true),
--           maxPreload = 2000,
--           preloadFileSize = 50000,
--         },
--         completion = { callSnippet = "Both" },
--         telemetry = { enable = false },
--       },
--     },
--   },
--   vimls = {},
--   dockerls = {},
--   omnisharp = {},
--   kotlin_language_server = {},
--   emmet_ls = {},
--   marksman = {},
-- }


-- mason_lsp_config.setup {
-- 	ensure_installed = vim.tbl_keys(servers),
-- 	automatic_installation = false,
-- }

-- -- Package installation folder
-- local install_root_dir = vim.fn.stdpath "data" .. "/mason"

-- mason_lsp_config.setup_handlers {
-- 	function(server_name)
-- 		local opts = vim.tbl_deep_extend("force", options, servers[server_name] or {})
-- 		lspconfig[server_name].setup { opts }
-- 	end,
-- 	["sumneko_lua"] = function()
-- 		local opts = vim.tbl_deep_extend("force", options, servers["sumneko_lua"] or {})
-- 		lspconfig.sumneko_lua.setup(require("lua-dev").setup { opts })
-- 	end,
-- }
