local M = {}

function M.setup()

	local status, cmp = pcall(require, "cmp")
	if not status then
		vim.notify("CMP is not installed", vim.log.levels.ERROR)
		return
	end

	local status2, luasnip = pcall(require, "luasnip")
	if not status2 then
		vim.notify("Luasnip is not installed", vim.log.levels.ERROR)
		return
	end


	local icons = require('volard.core.icons')


	cmp.setup({
		-- Load snippet support
		snippet = {
			expand = function(args)
				luasnip.lsp_expand(args.body)
			end
		},

		-- Completion settings
		completion = {
			completeopt = 'menu,menuone,noselect',
			keyword_length = 2
		},

		-- Key mapping
		mapping = {
			['<C-n>']     = cmp.mapping.select_next_item(),
			['<C-p>']     = cmp.mapping.select_prev_item(),
			['<C-d>']     = cmp.mapping.scroll_docs(-4),
			['<C-f>']     = cmp.mapping.scroll_docs(4),
			['<C-Space>'] = cmp.mapping.complete(),
			['<C-e>']     = cmp.mapping.close(),
			['<CR>']      = cmp.mapping.confirm {
				behavior = cmp.ConfirmBehavior.Replace,
				select = true,
			},

			-- Tab mapping
			['<Tab>'] = function(fallback)
				if cmp.visible() then
					cmp.select_next_item()
				elseif luasnip.expand_or_jumpable() then
					luasnip.expand_or_jump()
				else
					fallback()
				end
			end,
			['<S-Tab>'] = function(fallback)
				if cmp.visible() then
					cmp.select_prev_item()
				elseif luasnip.jumpable(-1) then
					luasnip.jump(-1)
				else
					fallback()
				end
			end
		},

		-- Load sources, see: https://github.com/topics/nvim-cmp
		sources = {
			{ name = 'nvim_lsp' },
			{ name = 'nvim_lua' },
			{ name = 'luasnip' },
			{ name = 'path' },
			{ name = 'buffer' },
			{ name = 'emoji' },
		},
		window = {
			completion = cmp.config.window.bordered(),
			documentation = cmp.config.window.bordered()
		},
		formatting = {
			format = function(entry, vim_item)
				-- Kind icons
				vim_item.kind = string.format('%s %s', icons.kind[vim_item.kind], vim_item.kind) -- This concatonates the icons with the name of the item kind
				-- Source
				vim_item.menu = ({
					buffer        = "[Buffer]",
					nvim_lsp      = "[LSP]",
					luasnip       = "[LuaSnip]",
					nvim_lua      = "[Lua]",
					latex_symbols = "[LaTeX]",
				})[entry.source.name]
				return vim_item
			end
		},
	})
end

return M
