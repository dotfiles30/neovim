local status_ok, mason_tool_installer = pcall(require, "mason-tool-installer")
if not status_ok then
	vim.notify("Mason haven't installed lol", vim.log.levels.ERROR)
	return
end

mason_tool_installer.setup {
  ensure_installed = 
		{ 
			"lua-language-server", 
			"tailwindcss"
			-- "stylua", 
			-- "omnisharp", 
			-- "clangd", 
			-- "css-lsp", 
			-- "pyright", 
			-- "marksman" 
	},
  auto_update = false,
  run_on_start = true,
}
