require('volard.core.options')
require('volard.core.autocmds')

if require "volard.core.first-load"() then
    return
end

require('volard.packer_init')
require('volard.plugins')
require('volard.lsp')
vim.g.Utils.keymap_setup(vim.g.Keymap.base_keymap)
