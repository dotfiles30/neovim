" ------------
" Sets
" ------------

" use :h <what_you_want_to_know_about> to figure out what's going on
" <silent> tells vim to show no message when this key sequence is used

syntax on
"set exrc           " looking for .vimrc in current working directory
"set secure         " needed for exrc setting
set number          " set line numbers
set noerrorbells    " turn off error's sounds
set encoding=UTF-8
set background=dark
set smartindent
set splitbelow
set splitright
set smartcase       " case sensitive searching
set incsearch       " highlight search result on the fly
set hidden          " keeps buffers open
set nohlsearch      " turn off remaining of searching results
set nowrap          " let strings go over the screen
set signcolumn=yes  " add extra column for notifications per lines
set cin             " some c lang indentings idk
set mousehide
set langmap=ФИСВУАПРШОЛДЬТЩЗЙКЫЕГМЦЧНЯ;ABCDEFGHIJKLMNOPQRSTUVWXYZ,фисвуапршолдьтщзйкыегмцчня;abcdefghijklmnopqrstuvwxyz


set autoindent      " copy indent from current line when starting a new line
set noexpandtab     " tab is tab char not spaces
"set expandtab       " tab chars is now spaces
set tabstop=4       " number of spaces a <Tab>
set shiftwidth=4    " make > do 4 spaces
"set softtabstop=4   " 4 spaces instead of tabs

set relativenumber
"set wrap linebreak nolist   " wrap lines by word not by letter
set modifiable              " make nerdtree buffer modifiable in order to create files, folders etc
set scrolloff=10            " start scroll earlier than cursor will go in the buttom
set guifont=JetBrains\ Mono:h17 " for windows actually

:imap jj <Esc>

" Ctrl+C and Ctrl+V to deal with system buffer
vmap <C-c> "+yi<ESC>
vmap <C-x> "+c<ESC>
vmap <C-v> c<ESC>"+p
nmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+<ESC>



" ------------
" Vim-plug stuff
" ------------
call plug#begin()
    Plug 'preservim/nerdtree'
    Plug 'lyokha/vim-xkbswitch'
    Plug 'sheerun/vim-polyglot' " a better syntax highlighting maybe...
    Plug 'pineapplegiant/spaceduck', { 'branch': 'main' } " colorscheme
    Plug 'vim-airline/vim-airline'
    Plug 'vim-airline/vim-airline-themes'
    Plug 'glepnir/dashboard-nvim'
    Plug 'neovim/nvim-lspconfig'
    Plug 'tpope/vim-commentary'

    Plug 'nvim-lua/plenary.nvim' " requirement for telescope
    Plug 'nvim-telescope/telescope.nvim'
    Plug 'mhinz/vim-startify'


    Plug 'ryanoasis/vim-devicons' " Always load this as the very last one.
call plug#end()

" ------------
" Commentary shortcuts
" ------------
"  gcc  - comment line
"  gcgc - uncomment line
"  gc   - in visual mode toggle comment a selection


" ------------
" Cpp special settings
" ------------

" for detecting OS
if !exists("g:os")
    if has("win64") || has("win32") || has("win16")
        let g:os = "Windows"
    else
        let g:os = substitute(system('uname'), '\n', '', '')
    endif
endif

nnoremap <C-^> :e #<CR>

" Execute cpp command (its for windows actually), startinsert required for closing terminal buffers
if g:os == "Windows"
	autocmd filetype cpp nnoremap <silent> <buffer> <Leader>che :6 split <Bar> execute 'terminal g++ % -o %:r && %:r.exe'          <Bar> startinsert<CR>
	autocmd filetype cpp nnoremap <silent> <buffer> <Leader>chf :6 split <Bar> execute 'terminal g++ % -o %:r && %:r.exe < in.txt' <Bar> startinsert<CR>
	autocmd filetype cpp nnoremap <silent> <buffer> <Leader>cve :50 vs   <Bar> execute 'terminal g++ % -o %:r && %:r.exe'          <Bar> startinsert<CR>
	autocmd filetype cpp nnoremap <silent> <buffer> <Leader>cvf :50 vs   <Bar> execute 'terminal g++ % -o %:r && %:r.exe < in.txt' <Bar> startinsert<CR>
endif

" Record macro q with Q
nnoremap Q @q
vnoremap Q :norm @q<cr>

" Shift + Direction to Change Tabs
noremap <S-l> gt
noremap <S-h> gT


" Close terminal buffers on leaving them when the process is done
augroup terminal
    autocmd!
    autocmd TermClose * if getline('$') == '[Process exited 0]' | close | endif
augroup end

" Close terminals when leave its buffers (not sure it works)
tnoremap <C-[> <C-\><C-n>
tnoremap <Esc> <C-\><C-n>

" Cpp snippet
nnoremap cpf i#include<bits/stdc++.h><Esc>o
             \#define ll long long<Esc>o
             \#define ull unsigned long long<Esc>o
             \#define nl "\n"<Esc>o
             \using namespace std;<Esc>o
             \<Esc>o
             \vector <string> solve(int count)<Esc>o
             \{<Esc>o
             \string in;<Esc>o
             \vector <string> res;<Esc>o
             \<Esc>o
             \return res;<Esc>o
             \}<Esc>o<Esc>o<Esc>o<CR>
             \int main(){<Esc>o
             \ios::sync_with_stdio(0);<Esc>o
             \cin.tie(0); cout.tie(0);<Esc>o
             \cout.setf(ios::fixed);<Esc>o
             \setprecision(10);<Esc>o
             \/* ====================== */<Esc>o
             \<Esc>o
             \int a; cin >> a;<Esc>o
             \<Esc>o
             \return 0;<Esc>o
             \}<Esc>
             \kki<Tab>


" ------------
" General remaps
" ------------

" Leader key to enter my command mode = <Space>
let mapleader = " "

" Splits navigation
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>

" Save on Ctrl-S
nmap <c-s> :w<CR>
imap <c-s> <Esc>:w<CR>a

" Indentation
vnoremap < <gv
vnoremap > >gv

" Resizing current window height
nnoremap <silent> <Leader>+ :exe "resize " . (winheight(0) * 3/2)<CR>
nnoremap <silent> <Leader>- :exe "resize " . (winheight(0) * 2/3)<CR>




" ------------
" NerdTree
" ------------

nnoremap <C-f> :NERDTreeToggle %<CR>

let NERDTreeShowHidden=1

" Autoreloading Nerd Tree on changes
"autocmd BufEnter NERD_tree_* | execute 'normal R'
"autocmd CursorHold * if exists("t:NerdTreeBufName") | call <SNR>15_refreshRoot() | endif

" Autorefresh Nerd Tree on change current directory
"augroup DIRCHANGE
"    au!
"    autocmd DirChanged global :NERDTreeCWD
"augroup END

" Open nerdtree window on opening Vim
"autocmd VimEnter * NERDTree





" ------------
" Telescope
" ------------

" Find files using Telescope command-line sugar.
nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>



" ------------
" Look
" ------------

" idk what's it but it was in colorscheme installation instructions
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

let g:airline_theme='lucius'

colorscheme spaceduck

" Font (its not always with backslash) - idk why it's not working for me
if has('gui_running')
    set guifont=JetBrains\ Mono:h18
endif


" ------------
" Dashboard
" ------------

" Fzf searcher for dashboard
let g:dashboard_default_executive ='telescope'

nmap     <Leader>ss :<C-u>SessionSave<CR>
nmap     <Leader>sl :<C-u>SessionLoad<CR>
nnoremap <silent> <Leader>fh :DashboardFindHistory<CR>
nnoremap <silent> <Leader>ff :DashboardFindFile<CR>
nnoremap <silent> <Leader>tc :DashboardChangeColorscheme<CR>
nnoremap <silent> <Leader>fa :DashboardFindWord<CR>
nnoremap <silent> <Leader>fb :DashboardJumpMark<CR>
nnoremap <silent> <Leader>cn :DashboardNewFile<CR>

"Use 24-bit (true-color) mode in Vim/Neovim when outside tmux.
"If you're using tmux version 2.2 or later, you can remove the outermost $TMUX check and use tmux's 24-bit color support
"(see < http://sunaku.github.io/tmux-24bit-color.html#usage > for more information.)
if (empty($TMUX))
  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
  endif
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif
endif


" air-line
let g:airline_powerline_fonts = 1

if !exists('g:airline_symbols')
    let g:airline_symbols = {}
endif

" unicode symbols
let g:airline_left_sep = '»'
let g:airline_left_sep = '▶'
let g:airline_right_sep = '«'
let g:airline_right_sep = '◀'
let g:airline_symbols.linenr = '␊'
let g:airline_symbols.linenr = '␤'
let g:airline_symbols.branch = '⎇'
let g:airline_symbols.paste = 'ρ'
let g:airline_symbols.paste = 'Þ'
let g:airline_symbols.paste = '∥'
let g:airline_symbols.whitespace = 'Ξ'

" airline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''



" ------------
" Startify
" ------------

" `SPC l s` - save current session
nnoremap <leader>ls :SSave<CR>

" `SPC l l` - list sessions / switch to different project
nnoremap <leader>ll :SClose<CR>


" 'Most Recent Files' number
let g:startify_files_number           = 18

" Update session automatically as you exit vim
let g:startify_session_persistence    = 1

" Simplify the startify list to just recent files and sessions
let g:startify_lists = [
  \ { 'type': 'dir',       'header': ['   Recent files'] },
  \ { 'type': 'sessions',  'header': ['   Saved sessions'] },
  \ ]

let g:startify_custom_header=[
    \'',
    \' ⣿⣿⣷⡁⢆⠈⠕⢕⢂⢕⢂⢕⢂⢔⢂⢕⢄⠂⣂⠂⠆⢂⢕⢂⢕⢂⢕⢂⢕⢂ ',
    \' ⣿⣿⣿⡷⠊⡢⡹⣦⡑⢂⢕⢂⢕⢂⢕⢂⠕⠔⠌⠝⠛⠶⠶⢶⣦⣄⢂⢕⢂⢕ ',
    \' ⣿⣿⠏⣠⣾⣦⡐⢌⢿⣷⣦⣅⡑⠕⠡⠐⢿⠿⣛⠟⠛⠛⠛⠛⠡⢷⡈⢂⢕⢂ ',
    \' ⠟⣡⣾⣿⣿⣿⣿⣦⣑⠝⢿⣿⣿⣿⣿⣿⡵⢁⣤⣶⣶⣿⢿⢿⢿⡟⢻⣤⢑⢂ ',
    \' ⣾⣿⣿⡿⢟⣛⣻⣿⣿⣿⣦⣬⣙⣻⣿⣿⣷⣿⣿⢟⢝⢕⢕⢕⢕⢽⣿⣿⣷⣔ ',
    \' ⣿⣿⠵⠚⠉⢀⣀⣀⣈⣿⣿⣿⣿⣿⣿⣿⣿⣿⣗⢕⢕⢕⢕⢕⢕⣽⣿⣿⣿⣿ ',
    \' ⢷⣂⣠⣴⣾⡿⡿⡻⡻⣿⣿⣴⣿⣿⣿⣿⣿⣿⣷⣵⣵⣵⣷⣿⣿⣿⣿⣿⣿⡿ ',
    \' ⢌⠻⣿⡿⡫⡪⡪⡪⡪⣺⣿⣿⣿⣿⣿⠿⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⣿⠃ ',
    \' ⠣⡁⠹⡪⡪⡪⡪⣪⣾⣿⣿⣿⣿⠋⠐⢉⢍⢄⢌⠻⣿⣿⣿⣿⣿⣿⣿⣿⠏⠈ ',
    \' ⡣⡘⢄⠙⣾⣾⣾⣿⣿⣿⣿⣿⣿⡀⢐⢕⢕⢕⢕⢕⡘⣿⣿⣿⣿⣿⣿⠏⠠⠈ ',
    \' ⠌⢊⢂⢣⠹⣿⣿⣿⣿⣿⣿⣿⣿⣧⢐⢕⢕⢕⢕⢕⢅⣿⣿⣿⣿⡿⢋⢜⠠⠈ ',
    \' ⠄⠁⠕⢝⡢⠈⠻⣿⣿⣿⣿⣿⣿⣿⣷⣕⣑⣑⣑⣵⣿⣿⣿⡿⢋⢔⢕⣿⠠⠈ ',
    \' ⠨⡂⡀⢑⢕⡅⠂⠄⠉⠛⠻⠿⢿⣿⣿⣿⣿⣿⣿⣿⣿⡿⢋⢔⢕⢕⣿⣿⠠⠈ ',
    \' ⠄⠪⣂⠁⢕⠆⠄⠂⠄⠁⡀⠂⡀⠄⢈⠉⢍⢛⢛⢛⢋⢔⢕⢕⢕⣽⣿⣿⠠⠈ ',
    \'',
    \]





" ------------
" Test area
" ------------

au TextChanged,TextChangedI <buffer> if &readonly == 0 && filereadable(bufname('%')) | silent write | endif
