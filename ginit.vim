" set guifont=JetBrainsMono\ Nerd\ Font:h16 - https://github.com/ryanoasis/nerd-fonts/issues/619
set guifont=FiraCode\ Nerd\ Font\ Mono:h13
set mouse=a
nnoremap "<S-l>" "gt"
nnoremap "<S-h>" "gT"
call GuiWindowMaximized(1)
